from tkinter import CASCADE
from django.db import models
from django.forms import CharField


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
# Create your models here.

class Hats(models.Model):
    color = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    picture_URL = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete = models.CASCADE
    )