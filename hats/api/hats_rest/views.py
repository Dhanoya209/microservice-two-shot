from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json




# Create your views here.

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "color",
        "style_name",
        "fabric",
        "location"
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties =[
        "style_name",
        "location"
    ]

class LocationEncoder(ModelEncoder):
    model= LocationVO
    properties=[
        "name",
        "import_href",
    ]
    


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.object.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.object.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.filter(id=pk).delete()
            return JsonResponse (
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            if "location" in content:
                content["location"] = LocationVO.objects.get(id=content["location"])
            Hats.object.filter(id=pk).update(**content)
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

